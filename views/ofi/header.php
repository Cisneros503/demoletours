<?php 
//$rr2 = "../";

if(!isset($_SESSION['vsIdUsuario']))
  {
    header('Location: ../index.php');
   }else{
    
   }
?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Demole!Ofi</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo $r;?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo $r;?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo $r;?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo $r;?>bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $r;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo $r;?>dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>D</b>!</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Demole</b>tour.com</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>


      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          


          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">  </span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">No hay notificaciones</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> Bienvenido a nuestra plataforma!!
                    </a>
                  </li>
                  <li>
                    <!--
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Aviso! Muy pronto podras disfrutar<br>
                      de todas las opciones del sistema ya<br>
                      que actualmente el sistema está en<br>proceso de desarrollo
                    </a>
                  -->
                  </li>
                  
                </ul>
              </li>
              <!--<li class="footer"><a href="#">Ver todas</a></li>-->
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          




          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $r;?>dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['vsNombre'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
<!--+++++++++++ IMG USR -->
                <img src="<?php echo $r;?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  <?php echo $_SESSION['vsEmail'] ?>
                  <small>Perfil: <?php echo $_SESSION['vsNombre']." ".$_SESSION['vsApellido'] ?></small>
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="perfil.php" class="btn btn-default btn-flat">Perfil</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo $ppal;?>controller/salir.php" class="btn btn-default btn-flat">Salir</a>
                </div>
              </li>
            </ul>
          </li>

          <!-- Control Sidebar Toggle Button -->
          
        </ul>
      </div>





    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $r;?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $_SESSION['vsNombre']." ".$_SESSION['vsApellido'] ?></p>
          <?php if ($_SESSION['vsEstado']==1){
          echo "<a href='#'><i class='fa fa-circle text-success'></i> Activo</a>";
          }else{echo "<a href='#'><i class='fa fa-circle text-red'></i> Inactivo</a>";}
          ?>        
        </div>
      </div>
      <!-- search form -->
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU PRINCIPAL</li>
       
        <li>
        <a href="../controller/perfil.php"><i class="fa fa-user"></i> 
        <span>Mi Perfil</span></a>
        </li>
        <li>
        <a href="../controller/cperfilcta.php"><i class="fa fa-dollar"></i> 
        <span>Mis Cuentas</span></a>
        </li>
        <li>
        <a href="#"><i class="fa fa-users"></i> 
        <span>Mi Red</span></a>
        </li>
        <li>
        <a href="#"><i class="fa fa-plane"></i> 
        <span>Mis Tours</span></a>
        </li>
        <li>
          <a href="../views/ofi/pages/examples/registrar.php"><i class="fa fa-drivers-license-o"></i> 
          <span>Registrar</span></a>
        </li>
        <?php
          if ($_SESSION['vsPerfil']=='admin') {
            echo '<li>
            <a href="../views/ofi/pages/examples/buscarUsr.php"><i class="fa fa-check"></i> 
            <span>Activar</span></a>
            </li>';
          }
        ?>
       
    </section>
    <!-- /.sidebar -->
  </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    
    



