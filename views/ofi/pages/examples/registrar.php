<?php
session_start();
$r = "views/ofi/"; 
$r2= "views/demole/";
$ppal= "../../../../";

if ($_SESSION['vsPerfil']==null) {
  header('Location:' . $ppal . 'index.php');
}

/*
if(isset($_GET['rg'])){
  $remp = array(" ");
  $idu = str_replace($remp, "",$_GET['rg']);

  $id = decrypt($idu);
  require($ppal."model/conexion.php");
  require($ppal."model/Usuario.php");
  $coneccion = new conexion();
  
  $usr=new Usr();
  $cnn=$coneccion->Conectar();
  $link = $usr->usrNA($cnn,$id);
  

  var_dump($id);
  echo " - ";
  var_dump($link);
  var_dump($_GET['rg']);
  mysqli_close($cnn);
  if ($link->num_rows>=1) {
    echo "existe!";
  }
}else{
  header('location:index.php');
}

function decrypt ($string) {
    $key = "d3m0l302";
    return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($string), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
}
*/

?>

<!DOCTYPE html>
<html5>
<head>
  <html lang="es">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Registrar</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../../plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <style type="text/css">
    body{
      background-color: #20B2AA; 
    }

  </style>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition " >

<div class="register-box">
  <div class="register-logo">
    <a href="../../../../index.php"><b>Demole!</b>Tours</a>
  </div>

  <div class="register-box-body">
    <h4><p class="login-box-msg">Registrar nuevo usuario</p></h4>

    <form action="../../../../index.php?acc=reg" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="n1" required placeholder="Primer Nombre" >
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="n2" required placeholder="Segundo Nombre">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="a1" required placeholder="Primer Apellido">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="a2" required placeholder="Segundo Apellido">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="tel" class="form-control" name="dui" required placeholder="Dui/Cedula">
        <span class="glyphicon glyphicon-credit-card form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="tel" class="form-control" name="tel" required placeholder="Telefono" pattern="[0-9]{1,15}"
        title="Solo puede ingresar numeros!!">
        <span class="glyphicon glyphicon-phone-alt form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="tel" class="form-control" name="cel" required placeholder="Celular" pattern="[0-9]{1,15}"
        title="Solo puede ingresar numeros!!">
        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="em" required placeholder="Correo Electronico">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" id="psw" name="psw" required placeholder="Contraseña">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" id="psw2" name="psw2" required placeholder="confirmar contraseña">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>

      <div class="form-group">
          <select class="form-control" name="gen" required title="Seleccione el Genero de la persona!">
            <option value="" disabled selected>Seleccione Genero...</option>
            <option>Masculino</option>
            <option>Femenino</option>           
          </select>
      </div>
      <div class="form-group">
                <label>Fecha de nacimiento:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="date" class="form-control" name="fe" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                </div>
                <!-- /.input group -->
              </div>




      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <!--
            <label>
              <input type="checkbox"> Acepto los terminos <a href="#">terms</a>
            </label>
            -->
          </div>
        </div>

        <!-- /.col -->



        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Registrar
          </button>
        </div>
        <!-- /.col -->
      </div>

    </form>

    

    <u><a href="<?php echo $ppal; ?>index.php" class="text-center">ir a la pagina principal</a></b></u>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    });
  });
</script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })

    })

    })
  })
</script>

<script type="text/javascript">
  var password = document.getElementById("psw")
  , confirm_password = document.getElementById("psw2");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Contraseñas no coinciden");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

if(history.forward(1)){
history.replace(history.forward(1));
}
</script>

</body>
</html5>
