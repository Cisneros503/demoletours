<?php
session_start();
$r = "views/ofi/"; 
$r2= "views/demole/";
$ppal= "../../../../";

if ($_SESSION['vsPerfil']!='admin') {
  header('Location:' . $ppal . 'index.php');
}

?>

<!DOCTYPE html>
<html5>
<head>
  <html lang="es">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Buscar Usuario</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../../plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <style type="text/css">
    body{
      background-color:   #20B2AA; 
    }
    table {     
      text-align: center;
    }
  </style>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition " >

<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>Demole!</b>Tours</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Buscar usuario</p>

    <form action="buscarUsr.php" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="n1" placeholder="Primer Nombre">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="n2" placeholder="Segundo Nombre">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="a1" placeholder="Primer Apellido">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="a2" placeholder="Segundo Apellido">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      

      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <!--
            <label>
              <input type="checkbox"> Acepto los terminos <a href="#">terms</a>
            </label>
            -->
          </div>
        </div>

        <!-- /.col -->



        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Buscar
          </button>
        </div>
        <!-- /.col -->
      </div>
      <u><a href="<?php echo $ppal; ?>index.php" class="text-center">ir a la pagina principal</a></u>
    </form>

  </div>
  <!-- /.form-box -->
</div>
<!-- /.reg0ister-box -->
<div width="80%">
<?php 

require($ppal."model/conexion.php");
require($ppal."model/Usuario.php");
require($ppal."model/adm.php");

if (isset($_POST['n1'])) {
	$usr = new Usr();
	$coneccion = new conexion();
	$cnn=$coneccion->Conectar(); //$cnn es mi conexion activa
	$resultado=$usr->verificar($cnn,$_POST['n1'],$_POST['n2'],$_POST['a1'],$_POST['a2']);
}


if (isset($_POST['ac'])) {
      $coneccion = new conexion();
      $cnn=$coneccion->Conectar(); //$cnn es mi conexion activa
      $adm = new Adm();
      $id = decrypt($_POST['ac']);
      //var_dump($_POST['ac']);
      //echo "<br>";
      //var_dump($id);
      $active=$adm->active($cnn,$id);
      //echo $_SESSION['vsMensaje'];
      //echo "<script type=\'text/javascript\'>alert(\'Exito');</script>";
      }

/*
if(isset($_GET['acc'])){
  require($ppal."model/Adm.php");
  $acc=$_GET['acc'];
  switch ($acc) {
    case 'act':
      if (isset($_POST['ac'])) {
      $coneccion = new conexion();
      $cnn=$coneccion->Conectar(); //$cnn es mi conexion activa
      $adm = new Adm();
      $active=$adm->active($cnn,$_POST['ac']);
      }
      break;
    
    default:
      # code... 
      break;
  }
    

}*/
if (isset($_SESSION['vsMensaje'])){
echo @$_SESSION['vsMensaje'];
}
echo "<div class='table-responsive'>";
echo "<h1 align='center'><b>Usuarios</b></h1>";
echo "<br>";
echo '<table border="1" width="80%" class="" align="center">';
echo "<tr>";
	echo "<td><b>Nombre</b></td>";
	echo "<td><b>Segundo Nombre</b></td>";
	echo "<td><b>Apellido</b></td>";
	echo "<td><b>Segundo Apellido</b></td>";
	echo "<td><b>Edad</b></td>";
	echo "<td><b>Correo</b></td>";
	echo "<td><b>Dui</b></td>";
	echo "<td><b>Genero</b></td>";
	echo "<td><b>Telefono</b></td>";
	echo "<td><b>Celular</b></td>";
	echo "<td><b>Estado</b></td>";
	echo '<td colspan="2"><b>Acciones</b></td>';
echo "</tr>";

//var_dump($resultado);
if (isset($resultado)) {
	
	while($rows=$resultado->fetch_array())
	{
		echo "<tr>";
		echo "	<td>".$rows['nombre']."</td>";
		echo "	<td>".$rows['nombre2']."</td>";
		echo "	<td>".$rows['apellido']."</td>";
		echo "	<td>".$rows['apellido2']."</td>";
    if ($rows['fechaNac']=='0000-00-00') {
      @$edad= 'No Especificada';
    }elseif ($rows['fechaNac']!='0000-00-00') {
      $f=date('Y-m-d');
      $fNac=$rows['fechaNac'];
      @$edad= $f-$fNac;
    }
		echo "	<td>".$edad."</td>";
		echo "	<td>".$rows['correo']."</td>";
		echo "	<td>".$rows['dui']."</td>";
		echo "	<td>".$rows['genero']."</td>";
		echo "	<td>".$rows['celular']."</td>";
		echo "	<td>".$rows['telefono']."</td>";
    if ($rows['estado']==1) {
      $est = 'Activo';
    }elseif ($rows['estado']==0) {
      $est = 'Inactivo';
    }
		echo "	<td>".$est."</td>";
		$idUs = encrypt($rows['id_usuario']);

		echo "	<td><form action'buscarUsr.php?acc=act' method='post'>
    <input type='hidden' 
    value='"
    . $idUs .
    "' name='ac' />
    <input type='submit' value='Activar' />
    </form></td>";
    echo "  <td><form method='post'>
    <input type='hidden' 
    value='"
    . $idUs .
    "' name='mod' />
    
    </form></td>";
		echo "</tr>";	
    echo "</div>";
	}
}
function encrypt ($string) {
    $key = "d3m0l3";
    return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key))));
}
function decrypt ($string) {
    $key = "d3m0l3";
    return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($string), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
}
if (isset($_SESSION['vsMensaje'])){
unset($_SESSION['vsMensaje']);
}
?>
</table>
</center>

</div>

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html5>
