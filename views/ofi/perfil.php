<?php //var_dump($_SESSION['vsComision']);//php var_dump($rdp);
//setlocale(LC_MONETARY, 'en_US');

?>
<!--*********** Content Header (Page header) -->
    <section class="content-header" >
      <h1>
        V-Officce
        <small>Demole!</small>
      </h1>
    <!-- ubicar en cada pag -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-map-marker"></i> Inicio</a></li>
        <li class="active">Mi Perfil</li>
      </ol>
    </section>

    <!-- Profile content content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">1° Nivel</span>
              <span class="info-box-number">
              <?php echo $countRed ;?> 
              <small> usuarios</small>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa ion-card"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"> Mis Comisiones</span>
              <span class="info-box-number"><?php 
              setlocale(LC_MONETARY, 'en_US.UTF-8');
              echo money_format('%.2n', $_SESSION['vsComisiones']) . "\n";
              ?><small> </small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa ion-earth"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Mis Viajes</span>
              <span class="info-box-number">0</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-android-bar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Viajes de regalo</span>
              <span class="info-box-number">0 <small></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->







      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
          <!-- MAP & BOX PANE -->
          <div class="box box-success">
          <!--
          <b>Tu link:<b><br>
          <small>www.demoletour.com/index.php?rg=<?php //echo $id;?></small>
          -->
          </div>
          
          <!-- /.box -->
          


          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Usuarios de mi red</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget=""><i class="fa fa-times"></i></button>
              </div>
            </div>


            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Apellido</th>             
                    <th>Celular</th>
                    <th>Estado</th>
                    <th>Correo</th>
                    <th>Fecha de inscripción</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                    require("Red.php");
                  ?>                 
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->



        <div class="col-md-4" >
                  <p class="text-center">
                    <strong>Mi Meta para la Red ideal</strong>
                  </p>

                  <div class="progress-group">
                    <span class="progress-text">Nivel-1</span>
                    <span class="progress-number"><b><?php echo count($nivel1);$nivel=count($nivel1) * 100 / 7;?></b>/7</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" <?php echo 'style="'.'width: '.$nivel.'%"';?>></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Nivel-2</span>
                    <span class="progress-number"><b><?php echo count($nivel2);$nivel=count($nivel2) * 100 / 49;?></b>/49</span>


                    <div class="progress sm">
                      <div class="progress-bar progress-bar-purple" <?php echo 'style="'.'width: '.$nivel.'%"';?>></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Nivel-3</span>
                    <span class="progress-number"><b><b><?php echo count($nivel3);$nivel=count($nivel3) * 100 / 147;?></b>/147</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" <?php echo 'style="'.'width: '.$nivel.'%"';?>></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Nivel-4</span>
                    <span class="progress-number"><b><b><?php echo count($nivel4);$nivel=count($nivel4) * 100 / 441;?></b>/441</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" <?php echo 'style="'.'width: '.$nivel.'%"';?>></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Nivel-5</span>
                    <span class="progress-number"><b><?php echo count($nivel5);$nivel=count($nivel5) * 100 / 1323;?></b>/1,323</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" <?php echo 'style="'.'width: '.$nivel.'%"';?>></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
          </div>
          <!-- /.col -->

                

        



        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
